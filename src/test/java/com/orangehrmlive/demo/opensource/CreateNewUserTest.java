package com.orangehrmlive.demo.opensource;

import org.testng.Assert;

import com.orangehrmlive.demo.opensource.pages.LoginPage;

public class CreateNewUserTest {

	public void testCreatingNewUser() {
		LoginPage.doLogin();
		DashboardPage.navigateToAdminUsersTab();
		AdminPage.openAddUserPage();
		AddUserPage.addNewUser();
		AdminPage.logOut(); //mainPage.logOut() ???
		LoginPage.doLogin(read the credentials from a file);
		
		Assert.assertEquals(actualGreetingsMessage, expectedGreetingsMessage);
		
	}

}
