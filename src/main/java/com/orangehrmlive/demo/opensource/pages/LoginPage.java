package com.orangehrmlive.demo.opensource.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.qameta.allure.Step;

public class LoginPage {
	private WebDriver driver;
	
	@FindBy(css = "#txtUsername")
	private WebElement usernameField;
	
	@FindBy(css = "#txtPassword")
	private WebElement passwordField;
	
	@FindBy(css = "#btnLogin")
	private WebElement loginButton;

	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	@Step("Fill in Username and Password and log in")
	public void doLogin(String username, String password) {
		usernameField.sendKeys(username);
		passwordField.sendKeys(password);
		loginButton.click();
	}
	
	
}
