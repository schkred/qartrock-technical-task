package com.orangehrmlive.demo.opensource.tools;

import java.io.FileReader;
import java.io.Reader;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class CredentialsFileReader {
	
	public static List<CSVRecord> readFile(String pathToFile) {
		try {
			Reader in = new FileReader(pathToFile);
			CSVParser parser = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(in);
			return parser.getRecords();
		} catch (Exception e) {
			throw new RuntimeException("Could not read a file");
		}
	}
}
